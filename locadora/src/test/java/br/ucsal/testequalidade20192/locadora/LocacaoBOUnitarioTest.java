package br.ucsal.testequalidade20192.locadora;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyNoMoreInteractions;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.Cliente;
import br.ucsal.testequalidade20192.locadora.dominio.Locacao;
import br.ucsal.testequalidade20192.locadora.dominio.Modelo;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ LocacaoBO.class })
public class LocacaoBOUnitarioTest {

	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		Cliente cliente = new Cliente("0556968450", "Luiz luiz", "558412333");
		Modelo modelo = new Modelo("Modelo");
		Veiculo carro = new Veiculo("55d55d4d4", 2000, modelo, 20d);
		
		List<Veiculo> listaVeiculos = new ArrayList<>();
		listaVeiculos.add(carro);
		List<String> placas = new ArrayList<>();
		placas.add("dd464as6d4");
		
		Date data = new Date();
		Locacao locacao = new Locacao(cliente, listaVeiculos, data, 5);
		
		
		LocacaoBO locacaoBO = new LocacaoBO();
		LocacaoBO spy = spy(locacaoBO);
		
		mockStatic(ClienteDAO.class);
		when(ClienteDAO.obterPorCpf("0556968450")).thenReturn(cliente);
		
		mockStatic(VeiculoDAO.class);
		when(VeiculoDAO.obterPorPlaca("55d55d4d4")).thenReturn(carro);
		
		verifyPrivate(spy).invoke("locarVeiculos");

		verifyStatic(LocacaoBO.class);
		LocacaoBO.locarVeiculos("123", placas, data, 5);

		verifyNoMoreInteractions(LocacaoBO.class);
	}
}
